aiosqlite==0.20.0
aiohttp==3.10.0
aiolimiter>1.1.0<2.0
aiofiles==24.1.0
plotly>5.15.0<6.0
pandas==2.2.2
kaleido==0.2.1
